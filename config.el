(setq 
      ;; doom-variable-pitch-font (font-spec :family "Fira Sans")
      ;; doom-unicode-font (font-spec :family "Input Mono Narrow" :size 12)
      doom-font (font-spec :family "Roboto Mono for Powerline" :size 15)
      doom-big-font (font-spec :family "Roboto Mono for Powerline" :size 19))
